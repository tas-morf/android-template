package com.accedo.android.template.stub;

import com.accedo.android.template.model.UrlProvider;

public class StubUrlProvider implements UrlProvider {
    @Override
    public String allMoviesUrl() {
        return null;
    }
}

package com.accedo.android.template.controller.listener;

import com.accedo.android.template.model.bean.client.Movie;

/**
 * Listens for movie clicking events
 */
public interface OnMovieClickListener {
    void onMovieClicked(Movie movie);
}

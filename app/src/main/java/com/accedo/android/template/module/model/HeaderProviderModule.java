package com.accedo.android.template.module.model;

import com.accedo.android.template.model.header.HeaderProvider;
import com.accedo.android.template.model.header.PreferencesHeaderProvider;

import static com.accedo.android.template.module.model.SimplePreferencesModule.defaultSimplePreferences;

public class HeaderProviderModule {

    public static HeaderProvider headerProvider() {
        return new PreferencesHeaderProvider(defaultSimplePreferences());
    }
}

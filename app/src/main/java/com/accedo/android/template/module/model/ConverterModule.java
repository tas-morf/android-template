package com.accedo.android.template.module.model;

import com.fasterxml.jackson.databind.util.Converter;
import com.accedo.android.template.model.bean.client.Movie;
import com.accedo.android.template.model.converter.MoviesConverter;
import com.accedo.android.template.model.bean.server.ServerMoviesResponse;

import java.util.List;

public class ConverterModule {
    public static Converter<ServerMoviesResponse, List<Movie>> movieConverter() {
        return new MoviesConverter();
    }
}

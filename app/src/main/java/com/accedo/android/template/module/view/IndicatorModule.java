package com.accedo.android.template.module.view;

import com.accedo.android.template.view.indicator.MovieListIndicator;
import com.accedo.android.template.view.indicator.impl.MovieListViewIndicator;

import static com.accedo.android.template.module.ResourcesModule.resources;
import static com.accedo.android.template.module.view.ToasterModule.toaster;

public class IndicatorModule {
    public static MovieListIndicator movieListIndicator() {
        return new MovieListViewIndicator(resources(), toaster());
    }
}

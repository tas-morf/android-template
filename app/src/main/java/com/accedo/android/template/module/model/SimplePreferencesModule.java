package com.accedo.android.template.module.model;

import android.preference.PreferenceManager;

import com.accedo.android.template.model.persistence.SimpleAndroidPreferences;
import com.accedo.android.template.model.persistence.SimplePreferences;

import static com.accedo.android.template.module.ApplicationModule.applicationContext;

public class SimplePreferencesModule {
    public static SimplePreferences defaultSimplePreferences() {
        return new SimpleAndroidPreferences(PreferenceManager.getDefaultSharedPreferences(applicationContext()));
    }
}

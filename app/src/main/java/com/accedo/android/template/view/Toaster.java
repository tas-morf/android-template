package com.accedo.android.template.view;

/**
 * Shows toasts
 */
public interface Toaster {

    void showToast(String toastMessage);
}

package com.accedo.android.template.module.view;

import com.accedo.android.template.view.AndroidToaster;
import com.accedo.android.template.view.Toaster;

import static com.accedo.android.template.module.ApplicationModule.applicationContext;

public class ToasterModule {

    public static Toaster toaster() {
        return new AndroidToaster(applicationContext());
    }
}

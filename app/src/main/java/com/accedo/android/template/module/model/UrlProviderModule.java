package com.accedo.android.template.module.model;

import com.accedo.android.template.model.ResourcesUrlProvider;
import com.accedo.android.template.model.UrlProvider;

import static com.accedo.android.template.module.ResourcesModule.resources;

public class UrlProviderModule {

    public static UrlProvider urlProvider() {
        return new ResourcesUrlProvider(resources());
    }
}
